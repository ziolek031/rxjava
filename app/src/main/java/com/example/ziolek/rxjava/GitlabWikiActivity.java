package com.example.ziolek.rxjava;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.io.Serializable;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.Subject;

public class GitlabWikiActivity extends AppCompatActivity {

    private Button button;
    private Movie movie;

    public GitlabWikiActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gitlab_wiki);

        button = (Button) findViewById(R.id.buttonRun);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                demoObservableFrom();
//                demoObservableJust();
//                demoObservableDefer();
//                demoObservableInterval();
                demoObservableCreate();


            }
        });
    }




    private void demoObservableCreate() {
        Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(ObservableEmitter<Integer> emitter) throws Exception {
                emitter.onNext(1);
                emitter.onNext(2);
                emitter.onNext(3);
                emitter.onComplete();
            }
        }).subscribe(new Subject<Integer>() {
            @Override
            public boolean hasObservers() {
                return false;
            }

            @Override
            public boolean hasThrowable() {
                return false;
            }

            @Override
            public boolean hasComplete() {
                return false;
            }

            @Override
            public Throwable getThrowable() {
                return null;
            }

            @Override
            protected void subscribeActual(Observer<? super Integer> observer) {

            }

            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Integer integer) {
                Log.i("onNext", String.valueOf(integer));
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void demoObservableInterval() {
        Observable.interval(2, TimeUnit.SECONDS).subscribe(new Subject<Long>() {
            @Override
            public boolean hasObservers() {
                return false;
            }

            @Override
            public boolean hasThrowable() {
                return false;
            }

            @Override
            public boolean hasComplete() {
                return false;
            }

            @Override
            public Throwable getThrowable() {
                return null;
            }

            @Override
            protected void subscribeActual(Observer<? super Long> observer) {

            }

            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Long aLong) {
                if (aLong == 5)
                    blockingSubscribe();
                Log.i("onNext", String.valueOf(aLong));
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void demoObservableDefer() {
        movie = new Movie("Fast & Furious 8");
        Observable<Movie> movieObservable = Observable.defer(new Callable<ObservableSource<? extends Movie>>() {
            @Override
            public ObservableSource<? extends Movie> call() throws Exception {
                return Observable.just(movie);
            }
        });
        movie = new Movie("Guardian of Galaxy");
        movieObservable.subscribe(new Subject<Movie>() {
            @Override
            protected void subscribeActual(Observer<? super Movie> observer) {

            }

            @Override
            public boolean hasObservers() {
                return false;
            }

            @Override
            public boolean hasThrowable() {
                return false;
            }

            @Override
            public boolean hasComplete() {
                return false;
            }

            @Override
            public Throwable getThrowable() {
                return null;
            }

            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Movie movie) {
                Log.i("onNext", movie.name);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    class Movie{
        public String name;

        public Movie(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    private void demoObservableJust() {
        Observable.just("aa",1,3).subscribe(new Observer<Serializable>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(Serializable serializable) {
                Log.i("onNext", String.valueOf(serializable));
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void demoObservableFrom() {
        Observable.fromArray(11,22,33)
                .subscribe(new Observer<Integer>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Integer integer) {
                        Log.d("onNext.DEBUG", String.valueOf(integer));
                        Log.e("onNext.ERROR", String.valueOf(integer));
                        Log.i("onNext.INFO", String.valueOf(integer));
                        Log.v("onNext.VERBOSE", String.valueOf(integer));
                        Log.w("onNext.W", String.valueOf(integer));
                        Log.wtf("onNext.WTF", String.valueOf(integer));

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }
}
